import {Component} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
    selector   : 'app-root',
    templateUrl: './app.component.html',
    styleUrls  : ['./app.component.css']
})
export class AppComponent {
    // private apiUrl       = 'https://maracana-2017.herokuapp.com/matches';
    private apiUrl       = 'https://futbolsat.com/file.json';
            matches: any = [];
            array: any   = [];

    constructor(private http: Http) {
        this.getData();
    }

    getData() {
        this.http.get(this.apiUrl)
            .subscribe((res: Response) => {
                this.matches = this.toArray(res.json());
                console.log(this.matches);
            });
    }

    toArray(json) {
        var keys = Object.keys(json);
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            this.array.push(json[key]);
        }
        return this.array;
    }

    parseDate(date) {

        var days = {
            Sunday   : 'Domingo',
            Monday   : 'Lunes',
            Tuesday  : 'Martes',
            Wednesday: 'Miércoles',
            Thursday : 'Jueves',
            Friday   : 'Viernes',
            Saturday : 'Sábado'
        };

        var months = {
            1 : 'Enero',
            2 : 'Febrero',
            3 : 'Marzo',
            4 : 'Abril',
            5 : 'Mayo',
            6 : 'Junio',
            7 : 'Julio',
            8 : 'Agosto',
            9 : 'Septiembre',
            10: 'Octubre',
            11: 'Noviembre',
            12: 'Diciembre'
        };

        var arrayDate = date.split("-");
        var weekDay   = arrayDate[0];
        var day       = arrayDate[1];
        var month     = parseInt(arrayDate[2],10);
        var year      = arrayDate[3];

        return days[weekDay] + " " + day + " de " + months[month];
    }

}
